SDFS_OUT := "./bin/sdfs"
DNS_OUT := "./bin/dns"
API_OUT := "./api/api.pb.go"
API_PB_OUT := "./api/api_grpc.pb.go"
PKG := "."
SDFS_PKG_BUILD := "${PKG}/sdfs"
DNS_PKG_BUILD := "${PKG}/dns"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)

.PHONY: all api sdfs dns

all: api sdfs dns

api/api.pb.go: api/api.proto
	@protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative api/api.proto

api: api/api.pb.go

api_test:
	@protoc --go_out=. --go_opt=

dep: ## Get the dependencies
	@go get -v -d ./...

sdfs: dep api ## Build the binary file for sdfs
	@go build -v -o $(SDFS_OUT) $(SDFS_PKG_BUILD)

dns: dep api ## Build the binary file for dns
	@go build -v -o $(DNS_OUT) $(DNS_PKG_BUILD)

clean: ## Remove previous builds
	@rm $(SDFS_OUT) $(API_OUT)

help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
