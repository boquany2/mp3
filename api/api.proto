syntax = "proto3";

package api;

import "google/protobuf/timestamp.proto";

option go_package = "mp3/api";

enum Status {
    Alive = 0;
    Timeout = 1;
    Leaved = 2;
}

message Process {
    string ip = 1;
    int32 port = 2;
    google.protobuf.Timestamp joinTime = 3; // when the process is joined
    google.protobuf.Timestamp lastUpdateTime = 4; // when the process is pinged
    Status status = 5;
}

// each client write has a unique id
message WriteId {
    string ip = 1;
    int32 port = 2;
    google.protobuf.Timestamp createTime = 3;
}

// Failure Detector Ring messages
message PingMessage {
    repeated Process processes = 1;
}

message AckMessage {
    string received = 1;
}

message JoinMessage {
    Process process = 1;
}

message LeaveMessage {
    Process process = 1;
}

enum MessageType {
    Ping = 0;
    Ack = 1;
    Join = 2;
    Leave = 3;
}

message Metadata {
    MessageType type = 1;
    oneof message {
        PingMessage ping = 2;
        AckMessage ack = 3;
        JoinMessage join = 4;
        LeaveMessage leave = 5;
    }
}

// gRPC-SDFS service
enum ResponseStatus {
    OK = 0;
    ERROR = 1;
    NOT_FOUND = 2;
}

enum RequestType {
    GET = 0;
    PUT = 1;
    DELETE = 2;
    LOOKUP = 3;
    BULK_LOOKUP = 4;
    BULK_GET = 5;
}

message Sequence {
    google.protobuf.Timestamp time = 1; // leader join time
    int32 count = 2;                    // leader sequence number
}

message ReadRequest {
    string filename = 1;
    int32 version = 2;
    string localFilename = 4;
    optional Sequence seq = 3;
}

message ReadResponse {
    bytes data = 1;
    ResponseStatus status = 2;
    optional Sequence seq = 3;
}

message WriteRequest {
    string filename = 1;
    bytes data = 2;
    WriteId writeId = 3;
    optional Sequence seq = 4;
}

message WriteResponse {
    ResponseStatus status = 1;
}

message DeleteRequest {
    string filename = 1;
    optional Sequence seq = 2;
}

message DeleteResponse {
    ResponseStatus status = 1;
}

message LookupRequest {
    string filename = 1;
    optional Sequence seq = 2;
}

message LookupResponse {
    string ip = 1;
    int32 port = 2;
    ResponseStatus status = 3;
}

message LookupResponseList {
    repeated LookupResponse responses = 1;
}

message BulkReadRequest {
    string filename = 1;
    int32 numVersions = 2;
    string localFilename = 4;
    optional Sequence seq = 3;
}

message BulkReadResponse {
    bytes data = 1;
    ResponseStatus status = 2;
    optional Sequence seq = 3;
}

message BulkLookupRequest {
    repeated string filenames = 1;
    optional Sequence seq = 2;
}

message BulkLookupResponse {
    string ip = 1;
    int32 port = 2;
    repeated string missingFiles = 3;
}

message RouteToLeaderRequest {
    RequestType type = 1;
    string filename = 2; // figure out filename withour accesing the request
    oneof request {
        ReadRequest read = 3;
        WriteRequest write = 4;
        DeleteRequest delete = 5;
        LookupRequest lookup = 6;
        BulkLookupRequest bulkLookup = 7;
        BulkReadRequest bulkRead = 8;
    }
}

message RouteToLeaderResponse {
    ResponseStatus status = 1;
    oneof response {
        ReadResponse read = 2;
        WriteResponse write = 3;
        DeleteResponse delete = 4;
        LookupResponseList lookupList = 5;
        BulkLookupResponse bulkLookup = 6;
        BulkReadResponse bulkRead = 7;

        // placeholder response type (not used in leader & client communication)
        LookupResponse lookup = 8;
    }
}

message FileTransferRequest {
    repeated WriteRequest snapshots = 1;
}

message FileTransferResponse {
    string ip = 1;
    int32 port = 2;
    ResponseStatus status = 3;
}

service SDFSService {
    // client requests get/put/delete/lookup that are routed to a master node
    rpc RouteToLeader(RouteToLeaderRequest) returns (RouteToLeaderResponse) {}
    // get a file from replicas
    rpc Read(ReadRequest) returns (ReadResponse) {}
    // put a file to replicas
    rpc Write(WriteRequest) returns (WriteResponse) {}
    // delete a file from replicas
    rpc Delete(DeleteRequest) returns (DeleteResponse) {}
    // lookup a file from replicas
    rpc Lookup(LookupRequest) returns (LookupResponse) {}
    // bulk lookup files from replicas, responded with missing files
    rpc BulkLookup(BulkLookupRequest) returns (BulkLookupResponse) {}
    // transfer missing file from two machines
    rpc FileTransfer(FileTransferRequest) returns (FileTransferResponse) {}
    // bulk get files from replicas
    rpc BulkRead(BulkReadRequest) returns (BulkReadResponse) {}
}

message LookupLeaderRequest {}

message LookupLeaderResponse {
    string address = 1;
}

message UpdateLeaderRequest {
    Process leader = 1;
}

message UpdateLeaderResponse {
    ResponseStatus status = 1;
}

service DNSService {
    rpc Lookup(LookupLeaderRequest) returns (LookupLeaderResponse) {}
    rpc Update(UpdateLeaderRequest) returns (UpdateLeaderResponse) {}
}
