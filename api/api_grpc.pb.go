// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.5
// source: api/api.proto

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// SDFSServiceClient is the client API for SDFSService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SDFSServiceClient interface {
	// client requests get/put/delete/lookup that are routed to a master node
	RouteToLeader(ctx context.Context, in *RouteToLeaderRequest, opts ...grpc.CallOption) (*RouteToLeaderResponse, error)
	// get a file from replicas
	Read(ctx context.Context, in *ReadRequest, opts ...grpc.CallOption) (*ReadResponse, error)
	// put a file to replicas
	Write(ctx context.Context, in *WriteRequest, opts ...grpc.CallOption) (*WriteResponse, error)
	// delete a file from replicas
	Delete(ctx context.Context, in *DeleteRequest, opts ...grpc.CallOption) (*DeleteResponse, error)
	// lookup a file from replicas
	Lookup(ctx context.Context, in *LookupRequest, opts ...grpc.CallOption) (*LookupResponse, error)
	// bulk lookup files from replicas, responded with missing files
	BulkLookup(ctx context.Context, in *BulkLookupRequest, opts ...grpc.CallOption) (*BulkLookupResponse, error)
	// transfer missing file from two machines
	FileTransfer(ctx context.Context, in *FileTransferRequest, opts ...grpc.CallOption) (*FileTransferResponse, error)
	// bulk get files from replicas
	BulkRead(ctx context.Context, in *BulkReadRequest, opts ...grpc.CallOption) (*BulkReadResponse, error)
}

type sDFSServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSDFSServiceClient(cc grpc.ClientConnInterface) SDFSServiceClient {
	return &sDFSServiceClient{cc}
}

func (c *sDFSServiceClient) RouteToLeader(ctx context.Context, in *RouteToLeaderRequest, opts ...grpc.CallOption) (*RouteToLeaderResponse, error) {
	out := new(RouteToLeaderResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/RouteToLeader", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sDFSServiceClient) Read(ctx context.Context, in *ReadRequest, opts ...grpc.CallOption) (*ReadResponse, error) {
	out := new(ReadResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/Read", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sDFSServiceClient) Write(ctx context.Context, in *WriteRequest, opts ...grpc.CallOption) (*WriteResponse, error) {
	out := new(WriteResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/Write", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sDFSServiceClient) Delete(ctx context.Context, in *DeleteRequest, opts ...grpc.CallOption) (*DeleteResponse, error) {
	out := new(DeleteResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sDFSServiceClient) Lookup(ctx context.Context, in *LookupRequest, opts ...grpc.CallOption) (*LookupResponse, error) {
	out := new(LookupResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/Lookup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sDFSServiceClient) BulkLookup(ctx context.Context, in *BulkLookupRequest, opts ...grpc.CallOption) (*BulkLookupResponse, error) {
	out := new(BulkLookupResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/BulkLookup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sDFSServiceClient) FileTransfer(ctx context.Context, in *FileTransferRequest, opts ...grpc.CallOption) (*FileTransferResponse, error) {
	out := new(FileTransferResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/FileTransfer", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *sDFSServiceClient) BulkRead(ctx context.Context, in *BulkReadRequest, opts ...grpc.CallOption) (*BulkReadResponse, error) {
	out := new(BulkReadResponse)
	err := c.cc.Invoke(ctx, "/api.SDFSService/BulkRead", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SDFSServiceServer is the server API for SDFSService service.
// All implementations must embed UnimplementedSDFSServiceServer
// for forward compatibility
type SDFSServiceServer interface {
	// client requests get/put/delete/lookup that are routed to a master node
	RouteToLeader(context.Context, *RouteToLeaderRequest) (*RouteToLeaderResponse, error)
	// get a file from replicas
	Read(context.Context, *ReadRequest) (*ReadResponse, error)
	// put a file to replicas
	Write(context.Context, *WriteRequest) (*WriteResponse, error)
	// delete a file from replicas
	Delete(context.Context, *DeleteRequest) (*DeleteResponse, error)
	// lookup a file from replicas
	Lookup(context.Context, *LookupRequest) (*LookupResponse, error)
	// bulk lookup files from replicas, responded with missing files
	BulkLookup(context.Context, *BulkLookupRequest) (*BulkLookupResponse, error)
	// transfer missing file from two machines
	FileTransfer(context.Context, *FileTransferRequest) (*FileTransferResponse, error)
	// bulk get files from replicas
	BulkRead(context.Context, *BulkReadRequest) (*BulkReadResponse, error)
	mustEmbedUnimplementedSDFSServiceServer()
}

// UnimplementedSDFSServiceServer must be embedded to have forward compatible implementations.
type UnimplementedSDFSServiceServer struct {
}

func (UnimplementedSDFSServiceServer) RouteToLeader(context.Context, *RouteToLeaderRequest) (*RouteToLeaderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RouteToLeader not implemented")
}
func (UnimplementedSDFSServiceServer) Read(context.Context, *ReadRequest) (*ReadResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Read not implemented")
}
func (UnimplementedSDFSServiceServer) Write(context.Context, *WriteRequest) (*WriteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Write not implemented")
}
func (UnimplementedSDFSServiceServer) Delete(context.Context, *DeleteRequest) (*DeleteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedSDFSServiceServer) Lookup(context.Context, *LookupRequest) (*LookupResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Lookup not implemented")
}
func (UnimplementedSDFSServiceServer) BulkLookup(context.Context, *BulkLookupRequest) (*BulkLookupResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BulkLookup not implemented")
}
func (UnimplementedSDFSServiceServer) FileTransfer(context.Context, *FileTransferRequest) (*FileTransferResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FileTransfer not implemented")
}
func (UnimplementedSDFSServiceServer) BulkRead(context.Context, *BulkReadRequest) (*BulkReadResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BulkRead not implemented")
}
func (UnimplementedSDFSServiceServer) mustEmbedUnimplementedSDFSServiceServer() {}

// UnsafeSDFSServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SDFSServiceServer will
// result in compilation errors.
type UnsafeSDFSServiceServer interface {
	mustEmbedUnimplementedSDFSServiceServer()
}

func RegisterSDFSServiceServer(s grpc.ServiceRegistrar, srv SDFSServiceServer) {
	s.RegisterService(&SDFSService_ServiceDesc, srv)
}

func _SDFSService_RouteToLeader_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RouteToLeaderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).RouteToLeader(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/RouteToLeader",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).RouteToLeader(ctx, req.(*RouteToLeaderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SDFSService_Read_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).Read(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/Read",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).Read(ctx, req.(*ReadRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SDFSService_Write_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WriteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).Write(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/Write",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).Write(ctx, req.(*WriteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SDFSService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).Delete(ctx, req.(*DeleteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SDFSService_Lookup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LookupRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).Lookup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/Lookup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).Lookup(ctx, req.(*LookupRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SDFSService_BulkLookup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BulkLookupRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).BulkLookup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/BulkLookup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).BulkLookup(ctx, req.(*BulkLookupRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SDFSService_FileTransfer_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(FileTransferRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).FileTransfer(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/FileTransfer",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).FileTransfer(ctx, req.(*FileTransferRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _SDFSService_BulkRead_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BulkReadRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SDFSServiceServer).BulkRead(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.SDFSService/BulkRead",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SDFSServiceServer).BulkRead(ctx, req.(*BulkReadRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// SDFSService_ServiceDesc is the grpc.ServiceDesc for SDFSService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var SDFSService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "api.SDFSService",
	HandlerType: (*SDFSServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "RouteToLeader",
			Handler:    _SDFSService_RouteToLeader_Handler,
		},
		{
			MethodName: "Read",
			Handler:    _SDFSService_Read_Handler,
		},
		{
			MethodName: "Write",
			Handler:    _SDFSService_Write_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _SDFSService_Delete_Handler,
		},
		{
			MethodName: "Lookup",
			Handler:    _SDFSService_Lookup_Handler,
		},
		{
			MethodName: "BulkLookup",
			Handler:    _SDFSService_BulkLookup_Handler,
		},
		{
			MethodName: "FileTransfer",
			Handler:    _SDFSService_FileTransfer_Handler,
		},
		{
			MethodName: "BulkRead",
			Handler:    _SDFSService_BulkRead_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api/api.proto",
}

// DNSServiceClient is the client API for DNSService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DNSServiceClient interface {
	Lookup(ctx context.Context, in *LookupLeaderRequest, opts ...grpc.CallOption) (*LookupLeaderResponse, error)
	Update(ctx context.Context, in *UpdateLeaderRequest, opts ...grpc.CallOption) (*UpdateLeaderResponse, error)
}

type dNSServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewDNSServiceClient(cc grpc.ClientConnInterface) DNSServiceClient {
	return &dNSServiceClient{cc}
}

func (c *dNSServiceClient) Lookup(ctx context.Context, in *LookupLeaderRequest, opts ...grpc.CallOption) (*LookupLeaderResponse, error) {
	out := new(LookupLeaderResponse)
	err := c.cc.Invoke(ctx, "/api.DNSService/Lookup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dNSServiceClient) Update(ctx context.Context, in *UpdateLeaderRequest, opts ...grpc.CallOption) (*UpdateLeaderResponse, error) {
	out := new(UpdateLeaderResponse)
	err := c.cc.Invoke(ctx, "/api.DNSService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DNSServiceServer is the server API for DNSService service.
// All implementations must embed UnimplementedDNSServiceServer
// for forward compatibility
type DNSServiceServer interface {
	Lookup(context.Context, *LookupLeaderRequest) (*LookupLeaderResponse, error)
	Update(context.Context, *UpdateLeaderRequest) (*UpdateLeaderResponse, error)
	mustEmbedUnimplementedDNSServiceServer()
}

// UnimplementedDNSServiceServer must be embedded to have forward compatible implementations.
type UnimplementedDNSServiceServer struct {
}

func (UnimplementedDNSServiceServer) Lookup(context.Context, *LookupLeaderRequest) (*LookupLeaderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Lookup not implemented")
}
func (UnimplementedDNSServiceServer) Update(context.Context, *UpdateLeaderRequest) (*UpdateLeaderResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedDNSServiceServer) mustEmbedUnimplementedDNSServiceServer() {}

// UnsafeDNSServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DNSServiceServer will
// result in compilation errors.
type UnsafeDNSServiceServer interface {
	mustEmbedUnimplementedDNSServiceServer()
}

func RegisterDNSServiceServer(s grpc.ServiceRegistrar, srv DNSServiceServer) {
	s.RegisterService(&DNSService_ServiceDesc, srv)
}

func _DNSService_Lookup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LookupLeaderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DNSServiceServer).Lookup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.DNSService/Lookup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DNSServiceServer).Lookup(ctx, req.(*LookupLeaderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DNSService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateLeaderRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DNSServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.DNSService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DNSServiceServer).Update(ctx, req.(*UpdateLeaderRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// DNSService_ServiceDesc is the grpc.ServiceDesc for DNSService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var DNSService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "api.DNSService",
	HandlerType: (*DNSServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Lookup",
			Handler:    _DNSService_Lookup_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _DNSService_Update_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api/api.proto",
}
