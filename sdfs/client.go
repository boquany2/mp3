package main

import (
	"bytes"
	"context"
	"fmt"
	"mp3/api"
	"mp3/logger"
	"mp3/ring"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"

	"google.golang.org/grpc"
)

/*
 * SDFS Client Implementation
 * This file includes all the functionalities supported by a SDFS client
 */

/*
 * SDFSClient interface
 * All the client (Terminal in this case) supported actions are defined here
 */

var DNS_ADDR = "127.0.0.1:8889"
var INTERVAL = 300 * time.Millisecond

type SDFSClientInterface interface {
	Get(SDFSFileName string, localFileName string) error
	Put(localFileName string, SDFSFileName string) error
	Delete(SDFSFileName string) error
	Ls(SDFSFileName string) error
	Store() error
	GetVersions(SDFSFileName string, versions int, localFileName string) error
	RouteToLeader() (*grpc.ClientConn, error)
}

type SDFSClient struct {
	SDFSClientInterface
	Ring      *ring.RingServer
	TaskQueue *Queue[*api.RouteToLeaderRequest] // task queue for client side
	FileTable *FileTable
}

func NewSDFSClient(ring *ring.RingServer, fileTable *FileTable) *SDFSClient {
	return &SDFSClient{
		TaskQueue: NewQueue[*api.RouteToLeaderRequest](),
		Ring:      ring,
		FileTable: fileTable,
	}
}

func (c *SDFSClient) Cron() {
	ticker := time.NewTicker(INTERVAL)

	for range ticker.C {
		if !c.TaskQueue.Empty() {
			req := c.TaskQueue.Top()

			t1 := time.Now()

			res, err := c.SendRequest(req)
			if err != nil {
				logger.Error("Failed to route request to leader while trying to send request " + req.GetFilename())
				logger.Error(err.Error())
				continue
			}

			if res.GetStatus() == api.ResponseStatus_ERROR {
				logger.Error("Response status is " + res.GetStatus().String() + " while trying to send request " + req.GetFilename())
				continue
			}

			c.HandleResponse(req, res)

			calculateTime(t1, req.GetType(), req.GetFilename())

			c.TaskQueue.Pop()
		}
	}
}

func (c *SDFSClient) HandleResponse(req *api.RouteToLeaderRequest, res *api.RouteToLeaderResponse) error {
	switch req.GetType() {
	case api.RequestType_GET:
		// handle not found error
		if res.GetStatus() == api.ResponseStatus_NOT_FOUND {
			fmt.Printf("File %s not found\n", req.GetRead().GetFilename())
			return nil
		}

		// Write to the file by its relative path
		absPath, _ := filepath.Abs("../data/" + c.Ring.Address() + "/" + req.GetRead().GetLocalFilename())
		err := os.WriteFile(absPath, res.GetRead().GetData(), 0644)
		if err != nil {
			fmt.Println("Failed to write file to local while trying to handle read response ")
			return err
		}
		fmt.Printf("File %s is written to local\n", req.GetRead().GetLocalFilename())

	case api.RequestType_BULK_GET:
		// handle not found error
		if res.GetStatus() == api.ResponseStatus_NOT_FOUND {
			fmt.Printf("File %s not found\n", req.GetRead().GetFilename())
			return nil
		}

		absPath, _ := filepath.Abs("../data/" + c.Ring.Address() + "/" + req.GetBulkRead().GetLocalFilename())
		err := os.WriteFile(absPath, res.GetBulkRead().GetData(), 0644)
		if err != nil {
			fmt.Println("Failed to write file to local while trying to handle bulk read response")
			return err
		}

	case api.RequestType_PUT:
		if res.GetStatus() == api.ResponseStatus_ERROR {
			fmt.Printf("Failed to put file %s", req.GetWrite().GetFilename())
			return nil
		}
		fmt.Println("Successfully put file " + res.GetWrite().GetStatus().String())

	case api.RequestType_DELETE:
		if res.GetStatus() == api.ResponseStatus_ERROR {
			fmt.Printf("Failed to delete file %s", req.GetDelete().GetFilename())
			return nil
		}
		fmt.Println("Successfully delete file " + res.GetDelete().GetStatus().String())

	case api.RequestType_LOOKUP:
		if res.GetStatus() == api.ResponseStatus_NOT_FOUND {
			fmt.Printf("File %s not found\n", req.GetLookup().GetFilename())
			return nil
		}
		for _, res := range res.GetLookupList().Responses {
			if res.GetStatus() == api.ResponseStatus_OK {
				fmt.Printf("File %s is stored in %s:%v\n", req.GetLookup().GetFilename(), res.GetIp(), res.GetPort())
			}
		}
	}

	return nil
}

func (c *SDFSClient) SendRequest(request *api.RouteToLeaderRequest) (*api.RouteToLeaderResponse, error) {
	leaderAddr, err := c.Ring.LookupLeader()
	if err != nil {
		return nil, err
	}

	// dial to leader
	conn, err := grpc.Dial(leaderAddr, GRPC_OPTIONS...)
	if err != nil {
		logger.Error("Failed to dial leader while trying to to send request " + request.GetFilename())
		return nil, err
	}
	defer conn.Close()

	// send request to leader
	client := api.NewSDFSServiceClient(conn)

	switch request.GetType() {
	case api.RequestType_BULK_GET:
		wg := sync.WaitGroup{}
		dataSlice := make([][]byte, int(request.GetBulkRead().GetNumVersions()))
		delimiter := []byte(":")

		for version := 1; version <= int(request.GetBulkRead().GetNumVersions()); version++ {
			wg.Add(1)

			go func(version int, dataSlice *[][]byte) {
				defer wg.Done()

				req, err := c.CreateGetRequest(request.GetBulkRead().GetFilename(), request.GetBulkRead().GetLocalFilename(), int32(version))
				if err != nil {
					logger.Error("Failed to create get request while trying to send bulk get request " + request.GetFilename())
					return
				}
				res, err := client.RouteToLeader(context.Background(), req)
				if err != nil || res.GetStatus() == api.ResponseStatus_ERROR {
					logger.Error("Failed to route request to leader while trying to send bulk get request " + request.GetFilename())
					return
				}
				if res.GetStatus() == api.ResponseStatus_NOT_FOUND {
					logger.Error("File not found while trying to send bulk get request " + request.GetFilename() + " version " + strconv.Itoa(version))
					return
				}
				(*dataSlice)[version-1] = res.GetRead().GetData()
			}(version, &dataSlice)
		}

		wg.Wait()
		var end int
		for end = len(dataSlice) - 1; end >= 0; end-- {
			if dataSlice[end] != nil {
				dataSlice = dataSlice[:end+1]
				break
			}
		}
		bulkData := bytes.Join(dataSlice, delimiter)
		logger.Info("bulk get request success " + request.GetFilename())

		return &api.RouteToLeaderResponse{
			Status: api.ResponseStatus_OK,
			Response: &api.RouteToLeaderResponse_BulkRead{
				BulkRead: &api.BulkReadResponse{
					Data:   bulkData,
					Status: api.ResponseStatus_OK,
				},
			},
		}, nil

	default:
		return client.RouteToLeader(context.Background(), request)
	}
}

func (c *SDFSClient) CreateGetRequest(SDFSFileName string, localFileName string, version int32) (*api.RouteToLeaderRequest, error) {

	request := api.RouteToLeaderRequest{
		Type:     api.RequestType_GET,
		Filename: SDFSFileName,
		Request: &api.RouteToLeaderRequest_Read{
			Read: &api.ReadRequest{
				Filename:      SDFSFileName,
				LocalFilename: localFileName,
				Version:       version,
			},
		},
	}

	return &request, nil
}

func (c *SDFSClient) CreatePutRequest(localFileName string, SDFSFileName string) (*api.RouteToLeaderRequest, error) {
	absPath, _ := filepath.Abs("../data/" + c.Ring.Address() + "/" + localFileName)

	fileBytes, err := os.ReadFile(absPath)
	if err != nil {
		errStr := "Failed to read file " + absPath
		logger.Error(errStr)
		return nil, err
	}

	writeId := &api.WriteId{
		Ip:         c.Ring.GetIp(),
		Port:       c.Ring.GetPort(),
		CreateTime: api.CurrentTimestamp(),
	}

	request := api.RouteToLeaderRequest{
		Type:     api.RequestType_PUT,
		Filename: SDFSFileName,
		Request: &api.RouteToLeaderRequest_Write{
			Write: &api.WriteRequest{
				Filename: SDFSFileName,
				Data:     fileBytes,
				WriteId:  writeId,
			},
		},
	}

	return &request, nil

}

func (c *SDFSClient) CreateDeleteRequest(SDFSFileName string) (*api.RouteToLeaderRequest, error) {
	request := api.RouteToLeaderRequest{
		Type:     api.RequestType_DELETE,
		Filename: SDFSFileName,
		Request: &api.RouteToLeaderRequest_Delete{
			Delete: &api.DeleteRequest{
				Filename: SDFSFileName,
			},
		},
	}

	return &request, nil

}

func (c *SDFSClient) CreateLsRequest(SDFSFileName string) (*api.RouteToLeaderRequest, error) {
	request := api.RouteToLeaderRequest{
		Type:     api.RequestType_LOOKUP,
		Filename: SDFSFileName,
		Request: &api.RouteToLeaderRequest_Lookup{
			Lookup: &api.LookupRequest{
				Filename: SDFSFileName,
			},
		},
	}
	return &request, nil
}

func (c *SDFSClient) Store() error {
	for name := range *c.FileTable {
		fmt.Println(name)
	}
	return nil
}

func (c *SDFSClient) CreateGetVersionsRequest(SDFSFileName string, versions int, localFileName string) (*api.RouteToLeaderRequest, error) {
	request := api.RouteToLeaderRequest{
		Type:     api.RequestType_BULK_GET,
		Filename: SDFSFileName,
		Request: &api.RouteToLeaderRequest_BulkRead{
			BulkRead: &api.BulkReadRequest{
				Filename:      SDFSFileName,
				NumVersions:   int32(versions),
				LocalFilename: localFileName,
			},
		},
	}
	return &request, nil
}

func calculateTime(t1 time.Time, reqType api.RequestType, filename string) {
	fmt.Printf("Total time taken for %v %v: %v\n", reqType.String(), filename, time.Duration(time.Since(t1).Nanoseconds()))
}
