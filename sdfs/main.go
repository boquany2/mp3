package main

import (
	"bufio"
	"fmt"
	"mp3/api"
	"mp3/logger"
	"mp3/ring"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/alexflint/go-arg"
	"google.golang.org/grpc"
)

var ServerArgs struct {
	Port int `arg:"-p" help:"port number" default:"8888"`
}
var IgnoredLogTypes = []string{logger.PING, logger.UPDATE}

func main() {
	arg.MustParse(&ServerArgs)
	port := ServerArgs.Port

	logger.Init(strconv.Itoa(port), IgnoredLogTypes)

	host, _ := os.Hostname()

	// create a UDP connection
	conn, err := net.ListenUDP("udp", &net.UDPAddr{
		Port: port,
		IP:   net.ParseIP(host),
	})
	if err != nil {
		logger.Error(err.Error())
		return
	}
	defer conn.Close()
	logger.Info("Process is listening on " + host + ":" + strconv.Itoa(port))

	// initialize SDFS server
	sdfs := NewSDFSServer()

	// ring failure detector
	ringServer := ring.NewRingServer(conn, host, int32(port), func(process *api.Process, action int) {
		sdfs.Lock()
		defer sdfs.Unlock()

		// callback for ring failure detector
		sdfs.AddSignal(action, process)
	})

	// initialize a ring failure detector with an additional SDFS related callback
	sdfs.Ring = ringServer

	// sdfs related grpc Server initialization
	sdfsClient := NewSDFSClient(ringServer, sdfs.FileTable)

	grpcServer := grpc.NewServer(
		grpc.MaxRecvMsgSize(MAX_BUFFER_SIZE),
		grpc.MaxSendMsgSize(MAX_BUFFER_SIZE),
	)
	api.RegisterSDFSServiceServer(grpcServer, sdfs)

	lis, err := net.Listen("tcp", ":"+strconv.Itoa(port))
	if err != nil {
		logger.Error("Failed to grpc Server listen: " + err.Error())
	}

	sdfs.ClearSDFSFiles()
	initDataFolder(sdfs.Ring.Address())

	// corn jobs
	go sdfs.Ring.Cron()
	go sdfsClient.Cron()
	go sdfs.Cron()

	// listening for incoming messages
	go sdfs.Ring.Listen()
	go attachToTerminal(sdfs, sdfsClient)

	if err := grpcServer.Serve(lis); err != nil {
		logger.Error("Failed to grpc Server serve: " + err.Error())
	}
}

/**
 * A function that attach a RingServer to terminal
 * - Read input from stdin
 * - Parse input
 * - Call appropriate function
 */
func attachToTerminal(s *SDFSServer, c *SDFSClient) {
	// read input from stdin
	reader := bufio.NewReader(os.Stdin)
	host, _ := os.Hostname()

	fmt.Println("Welcome to the SDFS CLI!")
	for {
		if c.TaskQueue.Len() > 0 {
			time.Sleep(1 * time.Second)
			continue
		}

		fmt.Printf("%v~$ ", host)
		text, _ := reader.ReadString('\n')
		text = text[:len(text)-1]
		args := strings.Fields(text)

		if len(args) == 0 {
			fmt.Println("Invalid command")
			continue
		}

		// ring commands
		switch args[0] {
		case "list_mem", "lm":
			s.Ring.ListMembers()
		case "list_self", "l":
			s.Ring.ListSelf()
		case "join":
			s.Ring.Join()
		case "leave":
			s.Ring.Leave()
		case "clear":
			logger.Init(strconv.Itoa(int(s.Ring.Port)), IgnoredLogTypes)
		case "stat":
			logger.Stats()
		case "low_droprate":
			ring.SetNetworkDropRate(0.03)
		case "mid_droprate":
			ring.SetNetworkDropRate(0.3)
		case "high_droprate":
			ring.SetNetworkDropRate(0.6)
		case "no_droprate":
			ring.SetNetworkDropRate(0.0)
		}

		// sdfs commands
		switch args[0] {
		case "get":
			if len(args) != 3 {
				fmt.Println("format: get sdfsfilename localfilename")
				continue
			}
			req, err := c.CreateGetRequest(args[1], args[2], LATEST_VERSION)
			if err != nil {
				fmt.Println("invalid argument, try again")
				continue
			}
			c.TaskQueue.Push(req)
		case "put":
			if len(args) != 3 {
				fmt.Println("format: put localfilename sdfsfilename")
				continue
			}
			req, err := c.CreatePutRequest(args[1], args[2])
			if err != nil {
				fmt.Println("invalid argument, try again")
				continue
			}
			c.TaskQueue.Push(req)
		case "delete":
			if len(args) != 2 {
				fmt.Println("format: delete sdfsfilename")
				continue
			}
			req, err := c.CreateDeleteRequest(args[1])
			if err != nil {
				fmt.Println("invalid argument, try again")
				continue
			}
			c.TaskQueue.Push(req)
		case "ls":
			if len(args) != 2 {
				fmt.Println("format: ls sdfsfilename")
				continue
			}
			req, err := c.CreateLsRequest(args[1])
			if err != nil {
				fmt.Println("invalid argument, try again")
				continue
			}
			c.TaskQueue.Push(req)
		case "store":
			if len(args) != 1 {
				fmt.Println("format: store")
				continue
			}
			c.Store()

		case "get-versions":
			if len(args) < 4 {
				fmt.Println("format: get-versions sdfsfilename num-versions localfilename")
				continue
			}

			vers, err := strconv.Atoi(args[2])

			if err != nil {
				fmt.Println("format: get-versions sdfsfilename num-versions localfilename")
				continue
			}

			req, err := c.CreateGetVersionsRequest(args[1], vers, args[3])
			if err != nil {
				fmt.Println("invalid argument, try again")
				continue
			}
			c.TaskQueue.Push(req)

		case "clear-sdfs":
			s.ClearSDFSFiles()

		case "clear-local":
			s.ClearLocalFiles()

		case "ls-local":
			s.ListLocalFiles()
		}
	}
}

func initDataFolder(addr string) {
	// Create data folder if not exist
	if _, err := os.Stat("../data"); os.IsNotExist(err) {
		err = os.Mkdir("../data", 0777)
		if err != nil {
			logger.Error("Failed to create directory data")
			panic(err)
		}
	}

	// Create folder by its address if not exist
	if _, err := os.Stat("../data/" + addr); os.IsNotExist(err) {
		err = os.Mkdir("../data/"+addr, 0777)
		if err != nil {
			logger.Error("Failed to create directory data")
			panic(err)
		}
	}
}
