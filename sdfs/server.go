package main

import (
	"context"
	"fmt"
	"math"
	"mp3/api"
	"mp3/lfu"
	"mp3/logger"
	"mp3/ring"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const REPLICA_COUNT = 4
const READ_CONSISTENCY = 2
const WRITE_CONSISTENCY = 3
const DELETE_CONSISTENCY = REPLICA_COUNT
const LOOKUP_CONSISTENCY = REPLICA_COUNT
const BULK_LOOKUP_CONSISTENCY = REPLICA_COUNT
const CHAN_SIZE = 256
const MAX_BUFFER_SIZE = 1.5 * lfu.GigaByte

// timeout
const GET_TIMEOUT = 3 * time.Second
const PUT_TIMEOUT = 15 * time.Second
const DELETE_TIMEOUT = 2 * time.Second
const LOOKUP_TIMEOUT = 2 * time.Second
const BULK_GET_TIMEOUT = 20 * time.Second

var GRPC_OPTIONS = []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(MAX_BUFFER_SIZE), grpc.MaxCallSendMsgSize(MAX_BUFFER_SIZE))}

type SignalEvent struct {
	EventType int
	*api.Process
}

type SDFSServer struct {
	FileTable             *FileTable           // file table
	FileCache             *lfu.LFUCache        // file cache
	Ring                  *ring.RingServer     // ring server
	HashRing              *HashRing            // ring for consistent hashing
	Signal                *Queue[*SignalEvent] // signal queue
	DeletePool            *Queue[string]       // delete pool
	SeqCounter            int                  // sequence counter
	sync.Mutex                                 // lock for concurrent access
	api.SDFSServiceServer                      // service interface
}

func NewSDFSServer() *SDFSServer {
	return &SDFSServer{
		FileTable:  NewFileTable(),
		FileCache:  lfu.NewLFUCache(100 * lfu.MegaByte),
		Signal:     NewQueue[*SignalEvent](),
		DeletePool: NewQueue[string](),
		HashRing:   NewHashRing(),
		SeqCounter: 0,
	}
}

func (server *SDFSServer) Cron() {
	for {
		time.Sleep(3 * time.Second)

		server.Recycle()
		server.Converge()
	}
}

func (server *SDFSServer) AddSignal(eventType int, process *api.Process) {
	server.Signal.Push(&SignalEvent{eventType, process})
}

/**
 * Read hashed file from local disk
 * @param filename - hashed filename from remote endpoint
 */
func (server *SDFSServer) ReadLocalFile(filename string) ([]byte, error) {
	dir := strconv.Itoa(int(server.Ring.Process.GetPort()))

	// open local file
	file, err := os.Open(dir + "/" + filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		return nil, err
	}

	data := make([]byte, info.Size())
	_, err = file.Read(data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

/**
 * Write data to local file
 * @param filename - hashed filename from remote endpoint
 * @param data - data to be written
 */
func (server *SDFSServer) WriteLocalFile(filename string, data []byte) error {
	dir := strconv.Itoa(int(server.Ring.Process.GetPort()))

	// check if dir exist
	_, err := os.Stat(dir)
	if err != nil {
		// create dir if not exist
		err := os.MkdirAll(dir, 0755)
		if err != nil {
			return err
		}
	}

	// create local file
	file, err := os.Create(dir + "/" + filename)
	if err != nil {
		return err
	}

	// writing to local file system
	if _, err = file.Write(data); err != nil {
		return err
	}

	return nil
}

func (server *SDFSServer) DeleteLocalFile(filename string) error {
	dir := strconv.Itoa(int(server.Ring.Process.GetPort()))

	// check if dir exist
	if _, err := os.Stat(dir); err != nil {
		return err
	}

	// delete local file
	if err := os.Remove(dir + "/" + filename); err != nil {
		return err
	}

	return nil
}

func (server *SDFSServer) ClearSDFSFiles() {
	dir := strconv.Itoa(int(server.Ring.Process.GetPort()))

	// check if dir exist
	if _, err := os.Stat(dir); err != nil {
		return
	}

	// list all files in dir
	files, err := os.ReadDir(dir)
	if err != nil {
		return
	}

	// delete all files in dir
	for _, file := range files {
		os.Remove(dir + "/" + file.Name())
	}
}

func (server *SDFSServer) ClearLocalFiles() {
	dir, _ := filepath.Abs("../data/" + server.Ring.Address() + "/")

	// check if dir exist
	if _, err := os.Stat(dir); err != nil {
		return
	}

	// list all files in dir
	files, err := os.ReadDir(dir)
	if err != nil {
		return
	}

	// delete all files in dir
	for _, file := range files {
		os.Remove(dir + "/" + file.Name())
	}
}

func (server *SDFSServer) ListLocalFiles() {
	dir, _ := filepath.Abs("../data/" + server.Ring.Address() + "/")

	// check if dir exist
	if _, err := os.Stat(dir); err != nil {
		return
	}

	// list all files in dir
	files, err := os.ReadDir(dir)
	if err != nil {
		return
	}

	// delete all files in dir
	for _, file := range files {
		fmt.Println(file.Name())
	}
}

/**
 * Get hash ring size-awared consistency level
 * @param reqType - request type
 * @return consistency level
 */
func (server *SDFSServer) GetConsistencyLevel(reqType api.RequestType) int {
	var level int

	switch reqType {
	case api.RequestType_GET:
		level = READ_CONSISTENCY
	case api.RequestType_PUT:
		level = WRITE_CONSISTENCY
	case api.RequestType_DELETE:
		level = DELETE_CONSISTENCY
	case api.RequestType_LOOKUP:
		level = LOOKUP_CONSISTENCY
	case api.RequestType_BULK_LOOKUP:
		level = BULK_LOOKUP_CONSISTENCY
	case api.RequestType_BULK_GET:
		level = READ_CONSISTENCY
	default:
		level = 0
	}

	return int(math.Min(float64(level), float64(server.HashRing.Len())))
}

func (server *SDFSServer) GetTimeout(reqType api.RequestType) time.Duration {
	var timeout time.Duration

	switch reqType {
	case api.RequestType_GET:
		timeout = GET_TIMEOUT
	case api.RequestType_PUT:
		timeout = PUT_TIMEOUT
	case api.RequestType_DELETE:
		timeout = DELETE_TIMEOUT
	case api.RequestType_LOOKUP:
		timeout = LOOKUP_TIMEOUT
	case api.RequestType_BULK_LOOKUP:
		timeout = BULK_GET_TIMEOUT
	default:
		timeout = 3 * time.Second
	}

	return timeout
}

func (server *SDFSServer) Recycle() {
	if !server.DeletePool.Empty() {
		server.Lock()
		fileName := server.DeletePool.Top()
		logger.Info(fmt.Sprintf("Deleting file %s...", fileName))
		server.DeletePool.Pop()
		server.Unlock()

		if err := server.DeleteLocalFile(fileName); err != nil {
			logger.Error(fmt.Sprintf("Failed to delete file %s: %v", fileName, err))
		}
	}

	stable := server.Signal.Len() == 0 && len(server.Ring.ExpirationPool) == 0
	if !stable {
		logger.Info("SDFS is not stable yet, stop file table scan...")
		return
	}

	server.Lock()
	defer server.Unlock()

	// refresh again just to make sure that convergence is ran after recycling
	server.HashRing.Refresh(server.Ring.MembershipList)
	for _, file := range server.FileTable.GetStoredFiles() {
		shouldDelete := true
		replicas := server.HashRing.FindReplicas(file, REPLICA_COUNT)
		for _, replica := range replicas {
			if api.IsSameProcess(replica, server.Ring.Process) {
				shouldDelete = false
				break
			}
		}

		if shouldDelete {
			for _, fv := range server.FileTable.GetVersions(file) {
				logger.Info(fmt.Sprintf("Adding file %v with version %v into delete pool", fv.ConcatName, fv.Seq))
				server.DeletePool.Push(fv.ConcatName)
			}

			// remove key from file table
			logger.Info("Removing file " + file + " from file table")
			server.FileTable.Delete(file)
		}
	}
}

func (server *SDFSServer) Converge() {
	// important to not adding lock here, otherwise it will pose a deadlock
	if !server.Signal.Empty() && server.Ring.MembershipList.Len() > 0 {
		if len(server.Ring.ExpirationPool) != 0 {
			logger.Info("Waiting for expiration pool to be empty...")
			return
		}

		logger.Info("Converging...")
		wg := sync.WaitGroup{}

		server.Lock()
		prevMainFiles := make([]string, 0)
		for _, file := range server.FileTable.GetStoredFiles() {
			if api.IsSameProcess(server.HashRing.GetRouteProcess(file), server.Ring.Process) {
				prevMainFiles = append(prevMainFiles, file)
			}
		}

		// important to refresh hash ring after getting prevMainFiles
		server.HashRing.Refresh(server.Ring.MembershipList)

		currMainFile := make([]string, 0)
		for _, file := range server.FileTable.GetStoredFiles() {
			if api.IsSameProcess(server.HashRing.GetRouteProcess(file), server.Ring.Process) {
				currMainFile = append(currMainFile, file)
			}
		}

		// check 1 predecessor
		predecessor := server.HashRing.FindPredecessor(server.Ring.Process)

		// check 3 successors
		successors := server.HashRing.FindSuccessors(server.Ring.Process, 3)
		server.Unlock()

		if predecessor != nil {
			logger.Info(fmt.Sprintf("Transfering files to predecessor %s...", predecessor.Address()))
			wg.Add(1)

			// file that is actually been routed to predecessor
			routedFiles := make([]string, 0)
			for _, file := range prevMainFiles {
				if api.IsSameProcess(server.HashRing.GetRouteProcess(file), predecessor) {
					routedFiles = append(routedFiles, file)
				}
			}
			go func(p *api.Process, files []string) {
				defer wg.Done()
				server.TransferFiles(p, files)
			}(predecessor, routedFiles)
		}

		logger.Info(fmt.Sprintf("Transfering files to %d successors...", len(successors)))
		for _, successor := range successors {
			wg.Add(1)

			go func(p *api.Process, files []string) {
				defer wg.Done()
				server.TransferFiles(p, files)
			}(successor, currMainFile)
		}

		wg.Wait()
		server.Lock()
		server.Signal.Clear()
		server.Unlock()

		logger.Info(server.HashRing.DebugValue())
		logger.Info("Server converged!")
	}
}

func (server *SDFSServer) TransferFiles(p *api.Process, files []string) {
	// dial to replica
	conn, err := grpc.Dial(p.Address(), GRPC_OPTIONS...)
	if err != nil {
		logger.Error(fmt.Sprintf("Failed to dial to %v while trying to converge: %v", p.Address(), err))
		return
	}
	defer conn.Close()

	// create client
	client := api.NewSDFSServiceClient(conn)

	res, err := client.BulkLookup(context.Background(), &api.BulkLookupRequest{
		Filenames: files,
	})
	if err != nil {
		logger.Error(fmt.Sprintf("Failed to send request to %v while trying to converge: %v", p.Address(), err))
		return
	}

	missingFiles := res.GetMissingFiles()
	if len(missingFiles) == 0 {
		logger.Info(fmt.Sprintf("No missing files for %s", p.Address()))
		return
	}

	snapshots := make([]*api.WriteRequest, 0)
	for _, file := range missingFiles {
		versions := server.FileTable.GetVersions(file)
		if len(versions) == 0 {
			logger.Error(fmt.Sprintf("Expected to find version for file %s, but found none", file))
			continue
		}

		for _, version := range versions {
			data, err := server.ReadLocalFile(version.ConcatName)
			if err != nil {
				logger.Error(fmt.Sprintf("Failed to read file %s while trying to converge: %v", version.ConcatName, err))
				continue
			}

			snapshots = append(snapshots, &api.WriteRequest{
				Filename: file,
				Data:     data,
				WriteId:  version.Id,
				Seq:      version.Seq,
			})
			logger.Write(len(data))
		}
	}

	logger.Info(fmt.Sprintf("Sending %d snapshots to %v...", len(snapshots), p.Address()))
	transferRes, err := client.FileTransfer(context.Background(), &api.FileTransferRequest{
		Snapshots: snapshots,
	})
	if err != nil {
		logger.Error(fmt.Sprintf("Failed to transfer files to %v while trying to converge: %v", p.Address(), err))
		return
	}
	if transferRes.GetStatus() != api.ResponseStatus_OK {
		logger.Error(fmt.Sprintf("Failed to transfer files to %v while trying to converge: %v", p.Address(), transferRes.GetStatus()))
		return
	}

	logger.Info(fmt.Sprintf("Successfully transferred %d files to %v", len(snapshots), p.Address()))
}
