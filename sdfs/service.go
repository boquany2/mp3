package main

import (
	"context"
	"errors"
	"fmt"
	"mp3/api"
	"mp3/lfu"
	"mp3/logger"
	"mp3/utils"
	"strconv"
	"time"

	"google.golang.org/grpc"
)

func (server *SDFSServer) RouteToLeader(ctx context.Context, req *api.RouteToLeaderRequest) (*api.RouteToLeaderResponse, error) {
	logger.Info("Received RouteToLeader request with type " + req.GetType().String())

	// unable to process routing request if ring is not converged
	stable := server.Signal.Len() == 0 && len(server.Ring.ExpirationPool) == 0
	if !stable {
		logger.Error("Unable to process routing request because ring is not converged")
		return &api.RouteToLeaderResponse{Status: api.ResponseStatus_ERROR, Response: nil}, nil
	}

	server.Lock()
	// increment sequence number
	server.SeqCounter++
	// get sequence struct
	seq := &api.Sequence{
		Time:  server.Ring.GetJoinTime(),
		Count: int32(server.SeqCounter),
	}
	// find replica set
	replicas := server.HashRing.FindReplicas(req.GetFilename(), REPLICA_COUNT)

	// DEBUG
	ports := make([]int32, 0)
	for _, replica := range replicas {
		ports = append(ports, replica.Port)
	}
	logger.Info(fmt.Sprintf("FindReplicas: hashKey=%v, replicas=%v", utils.Hash(req.GetFilename())%HASH_SIZE, ports))
	server.Unlock()

	signal := make(chan *api.RouteToLeaderResponse)
	for _, r := range replicas {
		// output is RouteToLeaderResponse so we can wrap message in it to generalize ack processing
		go func(r *api.Process) {
			res, err := server.SendLeaderRequest(req, seq, r)
			if err != nil || res.GetStatus() == api.ResponseStatus_ERROR {
				logger.Error(fmt.Sprintf("Failed to send RouteToLeader request to %v", r.Address()))
				return
			}
			signal <- res
		}(r)
	}

	// waiting for all acks in a certain consistency level
	ackRequired := server.GetConsistencyLevel(req.GetType())
	ackMessages := make([]*api.RouteToLeaderResponse, 0)
	ticker := time.NewTicker(server.GetTimeout(req.GetType()))

	logger.Info("Waiting for " + strconv.Itoa(ackRequired) + " acks...")
	for {
		// check if enough acks received
		if len(ackMessages) >= ackRequired {
			logger.Info("Received enough acks for " + req.GetType().String() + " request")
			return server.HandleLeaderResponse(req.GetType(), ackMessages)
		}

		select {
		case res := <-signal:
			logger.Info(fmt.Sprintf("Received ack %v", res.GetStatus()))
			// collect ack messages
			ackMessages = append(ackMessages, res)
		case <-ticker.C:
			// timeout waiting for ack
			logger.Error("Timeout waiting for acks")
			if req.GetType() == api.RequestType_GET || req.GetType() == api.RequestType_LOOKUP {
				return &api.RouteToLeaderResponse{Status: api.ResponseStatus_NOT_FOUND, Response: nil}, nil
			}
			return &api.RouteToLeaderResponse{Status: api.ResponseStatus_ERROR, Response: nil}, nil
		}
	}
}

func (server *SDFSServer) Read(ctx context.Context, req *api.ReadRequest) (*api.ReadResponse, error) {
	server.Lock()
	logger.Get(req.GetFilename(), int(req.GetVersion()))

	fv, ok := server.FileTable.Get(req.GetFilename(), int(req.GetVersion()))
	if !ok {
		// version not found
		server.Unlock()
		logger.Info("Trying to read a non-existing version of file " + req.GetFilename() + " with version " + strconv.Itoa(int(req.GetVersion())))
		return &api.ReadResponse{Status: api.ResponseStatus_ERROR}, fmt.Errorf("version not found")
	}

	if data, ok := server.FileCache.Get(lfu.DataKey(fv.ConcatName)); ok {
		// cache hit
		server.Unlock()
		logger.Info("File " + req.GetFilename() + " with version " + strconv.Itoa(int(req.GetVersion())) + " found in cache")
		return &api.ReadResponse{Status: api.ResponseStatus_OK, Data: data, Seq: fv.Seq}, nil
	}
	server.Unlock()

	// read from local file system if cache miss
	data, err := server.ReadLocalFile(fv.ConcatName)
	if err != nil {
		logger.Error("Failed to read file " + fv.ConcatName + ": " + err.Error())
		return &api.ReadResponse{Status: api.ResponseStatus_ERROR}, err
	}

	logger.Info("Read " + strconv.Itoa(len(data)) + " bytes from SDFS")
	return &api.ReadResponse{Status: api.ResponseStatus_OK, Data: data, Seq: fv.Seq}, nil
}

func (server *SDFSServer) Write(ctx context.Context, req *api.WriteRequest) (*api.WriteResponse, error) {
	logger.Put(req.GetFilename())

	server.Lock()
	concatFileName := utils.ConcatFilename(req.GetFilename(), req.GetSeq())

	// insert file into virtual file table
	err := server.FileTable.Insert(req.GetFilename(), FileVersion{
		ConcatName: concatFileName,
		Seq:        req.GetSeq(),
		Id:         req.GetWriteId(),
	})

	// duplicated write/seq id, ignore and return immediately
	if err != nil {
		server.Unlock()
		logger.Error(fmt.Sprintf("Duplicated write/seq id %v", req.GetWriteId()))
		return &api.WriteResponse{Status: api.ResponseStatus_OK}, nil
	}

	// insert file data into cache if file size is smaller than 10 MB
	if len(req.GetData()) <= 10*lfu.MegaByte {
		server.FileCache.Put(lfu.DataKey(concatFileName), req.GetData())
	}
	server.Unlock()

	// write to local file system
	err = server.WriteLocalFile(concatFileName, req.GetData())
	if err != nil {
		logger.Error("Fail to write file " + req.GetFilename() + ": " + err.Error())
		return &api.WriteResponse{Status: api.ResponseStatus_ERROR}, err
	}

	logger.Info("Written " + strconv.Itoa(len(req.GetData())) + " bytes into SDFS")
	logger.Info(fmt.Sprintf("Current latest version: %v", server.FileTable.NumVersions(req.GetFilename())))
	return &api.WriteResponse{Status: api.ResponseStatus_OK}, nil
}

func (server *SDFSServer) Delete(ctx context.Context, req *api.DeleteRequest) (*api.DeleteResponse, error) {
	logger.Remove(req.GetFilename())

	server.Lock()
	// check if file exists in file table
	if !server.FileTable.Contains(req.GetFilename()) {
		logger.Error("Trying to delete a non-existing file " + req.GetFilename())
		server.Unlock()
		return &api.DeleteResponse{Status: api.ResponseStatus_OK}, nil
	}

	// add file into delete pool for soft deletion
	for _, fv := range server.FileTable.GetVersions(req.GetFilename()) {
		logger.Info(fmt.Sprintf("Adding file %v with version %v into delete pool", fv.ConcatName, fv.Seq))
		server.DeletePool.Push(fv.ConcatName)
	}

	// remove key from file table
	logger.Info("Removing file " + req.GetFilename() + " from file table")
	server.FileTable.Delete(req.GetFilename())
	server.Unlock()

	return &api.DeleteResponse{Status: api.ResponseStatus_OK}, nil
}

func (server *SDFSServer) Lookup(ctx context.Context, req *api.LookupRequest) (*api.LookupResponse, error) {
	logger.Lookup(req.GetFilename())

	server.Lock()
	defer server.Unlock()

	if server.FileTable.Contains(req.GetFilename()) {
		logger.Info("File " + req.GetFilename() + " found in file table")
		return &api.LookupResponse{Status: api.ResponseStatus_OK, Ip: server.Ring.Ip, Port: server.Ring.Port}, nil
	}

	logger.Info("File " + req.GetFilename() + " not found in file table")
	return &api.LookupResponse{Status: api.ResponseStatus_ERROR}, fmt.Errorf("file %v not found", req.GetFilename())
}

func (server *SDFSServer) BulkLookup(ctx context.Context, req *api.BulkLookupRequest) (*api.BulkLookupResponse, error) {
	missingFiles := make([]string, 0)

	server.Lock()
	for _, filename := range req.GetFilenames() {
		logger.Lookup(filename)

		if !server.FileTable.Contains(filename) {
			logger.Info("File " + filename + " not found in file table")
			missingFiles = append(missingFiles, filename)
		} else {
			logger.Info("File " + filename + " found in file table")
		}
	}
	server.Unlock()

	return &api.BulkLookupResponse{
		Ip:           server.Ring.Process.Ip,
		Port:         server.Ring.Process.Port,
		MissingFiles: missingFiles,
	}, nil
}

func (server *SDFSServer) FileTransfer(ctx context.Context, req *api.FileTransferRequest) (*api.FileTransferResponse, error) {
	logger.Transfer(len(req.GetSnapshots()))

	server.Lock()
	defer server.Unlock()

	for _, snapshot := range req.GetSnapshots() {
		concatFileName := utils.ConcatFilename(snapshot.GetFilename(), snapshot.GetSeq())
		// insert file into virtual file table
		err := server.FileTable.Insert(snapshot.GetFilename(), FileVersion{
			ConcatName: concatFileName,
			Seq:        snapshot.GetSeq(),
			Id:         snapshot.GetWriteId(),
		})

		// duplicated write/seq id, ignore
		if err != nil {
			logger.Error(fmt.Sprintf("Duplicated write/seq id %v", snapshot.GetWriteId()))
			continue
		}

		// insert file data into cache
		server.FileCache.Put(lfu.DataKey(concatFileName), snapshot.GetData())

		// write to local file system
		err = server.WriteLocalFile(concatFileName, snapshot.GetData())
		if err != nil {
			logger.Error("Fail to write file " + snapshot.GetFilename() + ": " + err.Error())
			continue
		}

		logger.Info("Written " + strconv.Itoa(len(snapshot.GetData())) + " bytes into SDFS")
		logger.Info(fmt.Sprintf("Current latest version: %v", server.FileTable.NumVersions(snapshot.GetFilename())))
	}

	return &api.FileTransferResponse{Status: api.ResponseStatus_OK}, nil
}

func (server *SDFSServer) HandleLeaderResponse(reqType api.RequestType, responses []*api.RouteToLeaderResponse) (*api.RouteToLeaderResponse, error) {
	if len(responses) == 0 {
		logger.Error("No acks received")
		return &api.RouteToLeaderResponse{Status: api.ResponseStatus_ERROR, Response: nil}, nil
	}

	// post-process leader response
	res := &api.RouteToLeaderResponse{Status: api.ResponseStatus_OK, Response: nil}
	switch reqType {
	case api.RequestType_GET:
		// get latest version
		latest := responses[0].GetRead().GetSeq()
		data := responses[0].GetRead().GetData()

		for _, res := range responses {
			if latest.Less(res.GetRead().GetSeq()) {
				latest = res.GetRead().GetSeq()
				data = res.GetRead().GetData()
			}
		}

		res.Response = &api.RouteToLeaderResponse_Read{
			Read: &api.ReadResponse{
				Status: api.ResponseStatus_OK,
				Data:   data,
				Seq:    latest,
			},
		}
		return res, nil

	case api.RequestType_PUT:
		res.Response = &api.RouteToLeaderResponse_Write{
			Write: &api.WriteResponse{
				Status: api.ResponseStatus_OK,
			},
		}
		return res, nil

	case api.RequestType_DELETE:
		res.Response = &api.RouteToLeaderResponse_Delete{
			Delete: &api.DeleteResponse{
				Status: api.ResponseStatus_OK,
			},
		}
		return res, nil

	case api.RequestType_LOOKUP:
		lookups := make([]*api.LookupResponse, 0)

		for _, res := range responses {
			lookups = append(lookups, res.GetLookup())
		}

		res.Response = &api.RouteToLeaderResponse_LookupList{
			LookupList: &api.LookupResponseList{
				Responses: lookups,
			},
		}
		return res, nil

	default:
		return &api.RouteToLeaderResponse{Status: api.ResponseStatus_ERROR, Response: nil}, nil
	}
}

/*
 * SendLeaderRequest distribute the RouteToLeaderRequest to a specific process
 *
 */
func (server *SDFSServer) SendLeaderRequest(req *api.RouteToLeaderRequest, seq *api.Sequence, r *api.Process) (*api.RouteToLeaderResponse, error) {
	// make grpc connection
	conn, err := grpc.Dial(r.Address(), GRPC_OPTIONS...)
	if err != nil {
		logger.Error("Failed to dial " + r.Address() + ": " + err.Error())
		return nil, err
	}
	defer conn.Close()

	// create grpc client
	client := api.NewSDFSServiceClient(conn)

	// attach current sequence struct to request & send request
	switch req.GetType() {
	case api.RequestType_GET:
		req.GetRead().Seq = seq
		res, err := client.Read(context.Background(), req.GetRead())
		if err != nil || res.GetStatus() == api.ResponseStatus_ERROR {
			logger.Error("Failed to read file " + req.GetRead().GetFilename() + " from " + r.Address() + ": " + err.Error())
			return nil, fmt.Errorf("failed to read file %s from %s: %v", req.GetRead().GetFilename(), r.Address(), err)
		}
		return &api.RouteToLeaderResponse{Response: &api.RouteToLeaderResponse_Read{Read: res}}, nil

	case api.RequestType_PUT:
		req.GetWrite().Seq = seq
		res, err := client.Write(context.Background(), req.GetWrite())
		if err != nil || res.GetStatus() == api.ResponseStatus_ERROR {
			logger.Error("Failed to write file " + req.GetWrite().GetFilename() + " to " + r.Address() + ": " + err.Error())
			return nil, fmt.Errorf("failed to write file %s to %s: %v", req.GetWrite().GetFilename(), r.Address(), err)
		}
		return &api.RouteToLeaderResponse{Response: &api.RouteToLeaderResponse_Write{Write: res}}, nil

	case api.RequestType_DELETE:
		req.GetDelete().Seq = seq
		res, err := client.Delete(context.Background(), req.GetDelete())
		if err != nil || res.GetStatus() == api.ResponseStatus_ERROR {
			logger.Error("Failed to delete file " + req.GetDelete().GetFilename() + " from " + r.Address() + ": " + err.Error())
			return nil, fmt.Errorf("failed to delete file %s from %s: %v", req.GetDelete().GetFilename(), r.Address(), err)
		}
		return &api.RouteToLeaderResponse{Response: &api.RouteToLeaderResponse_Delete{Delete: res}}, nil

	case api.RequestType_LOOKUP:
		req.GetLookup().Seq = seq
		res, err := client.Lookup(context.Background(), req.GetLookup())
		if err != nil || res.GetStatus() == api.ResponseStatus_ERROR {
			logger.Error("Failed to lookup file " + req.GetLookup().GetFilename() + " from " + r.Address() + ": " + err.Error())
			return nil, fmt.Errorf("failed to lookup file %s from %s: %v", req.GetLookup().GetFilename(), r.Address(), err)
		}
		return &api.RouteToLeaderResponse{Response: &api.RouteToLeaderResponse_Lookup{Lookup: res}}, nil

	default:
		logger.Error("Leader: receive invalid request type")
		return nil, errors.New("invalid request type")
	}
}
