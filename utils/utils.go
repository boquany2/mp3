package utils

import (
	"fmt"
	"hash/fnv"
	"math/rand"
	"mp3/api"
	"time"
)

var source = rand.NewSource(time.Now().UnixNano())
var r = rand.New(source)

/*
 * Drop a message with a given probability
 *
 * @param prob: probability of dropping a message
 * @param callback: callback function to write a message
 * @return int: number of bytes written
 * @return error: raise error if writing fails
 */
func WithDropProb(prob float64, callback func() (int, error)) (int, error) {
	if r.Float64() <= prob {
		return 0, fmt.Errorf("network packet dropped, fail probability: %v", prob)
	}
	return callback()
}

func ConcatFilename(filename string, seq *api.Sequence) string {
	return fmt.Sprintf("[%v][%v][%d]", filename, seq.GetTime().AsTime(), seq.GetCount())
}

func Hash(key string) int {
	h := fnv.New32a()
	h.Write([]byte(key))
	return int(h.Sum32())
}
